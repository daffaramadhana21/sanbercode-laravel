<?php

// panggil controller
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// akses halaman menggunakan controller
Route::get('/', [HomeController::class, 'home']);
Route::get('/register', [AuthController::class, 'register']);
Route::get('/welcome', [AuthController::class, 'welcome']);
Route::post('/welcome', [AuthController::class, 'welcome']);

Route::get('/datatable', function(){
    return view('page.datatable');
});

Route::get('table', function(){
    return view('page.table');
});

// testing halaman template
// Route::get('/master', function(){
//     return view('layout.master');
// });