@extends('layout.master')

@section('title')
Buat Account Baru !
@endsection

@section('content')
<h2>Sign Up Form</h2>
<form action="/welcome" method="POST">
    @csrf
    
    <p>First Name :</p>
    <input type="text" name="fName">
    
    <p>Last Name :</p>
    <input type="text" name="lName">
    
    <p>Gender :</p>
    <input type="radio"> Male
    <input type="radio"> Female
    
    <p>Nationality :</p>
    <select name="Nationality" id="">
        <option value="indonesia">indonesia</option>
        <option value="malaysia">malaysia</option>
    </select>
    
    <p>Language Spoken :</p>
    <input type="checkbox">Bahasa Indoenesia
    <br>
    <input type="checkbox">English
    <br>
    <input type="checkbox">Other
    
    <p>Bio :</p>
    <textarea name="bio" id="" cols="30" rows="10"></textarea> 
    
    <br>
    <br>
    
    <a href="/kirim">
        <button>Sign Up</button>
    </a>
</form>
</body>
@endsection