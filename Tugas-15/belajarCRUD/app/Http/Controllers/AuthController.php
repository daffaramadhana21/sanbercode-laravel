<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class AuthController extends Controller
{
    public function register()
    {
        return view('page.form');
    }

    public function welcome(Request $request)
    {
        $namaDepan = $request['fName'];
        $namaBelakang = $request['lName'];

        return view('page.welcome', [
            'namaDepan' => $namaDepan,
            'namaBelakang' => $namaBelakang,

        ]);
    }
}