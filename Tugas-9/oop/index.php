<?php 
    require('animal.php');

    $binatang = new Animal("Shaun");
    echo "Nama hewan : " . $binatang->name . '<br>';
    echo "Legs : " . $binatang->legs . '<br>';
    echo "Cold Blooded : " . $binatang->cold_blooded . '<br><br>';
    
    require('frog.php');

    $kodok = new Frog("Buduk");
    echo "Nama hewan : " . $kodok->name . '<br>';
    echo "Legs : " . $kodok->legs . '<br>';
    echo "Cold Blooded : " . $kodok->cold_blooded . '<br>';
    echo "Jump : " . $kodok->jump . '<br><br>';

    require('ape.php');

    $sungokong = new Ape("Kera");
    echo "Nama hewan : " . $sungokong->name . '<br>';
    echo "Legs : " . $sungokong->legs . '<br>';
    echo "Cold Blooded : " . $sungokong->cold_blooded . '<br>';
    echo "Yell : " . $sungokong->yell . '<br>';
    
    
?>