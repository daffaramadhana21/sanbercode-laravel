<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('pages.form');
    }

    public function welcome(Request $request)
    {
        $namaDepan = $request['fName'];
        $namaBelakang = $request['lName'];
        
        // dd($request->all());
        
        return view('pages.welcome', 
        
        [
            "namaDepan"     => $namaDepan, 
            "namaBelakang"  => $namaBelakang
        ]);
    }
}